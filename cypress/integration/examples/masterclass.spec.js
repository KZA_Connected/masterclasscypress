describe("Masterclass cypress", () => {
  beforeEach(function() {
    cy.visit(
      "http://cursusclient-ontwikkel-hco-kza-connected.online.hotcontainers.nl/cursussen"
    );
  });

  it("Hier je test", function() {
    cy.get("h2").should("have.text", "Welkom bij KZA Connected");
  });
});
